---
layout: page
title: 你知道平面设计是什么吗？
excerpt_separator: "<!--more-->"
categories:
     - 平面设计
---

 平面设计的一些知识

<!--more-->

**一、平面设计是什么？**

平面设计（graphic design）的定义泛指具有艺术性和专业性，以“视觉”作为沟通和表现的方式。

平面设计是沟通传播、风格化和通过文字和图像解决问题的艺术。

**二、平面设计主要做什么？**

平面设计主要做美术排版、平面广告、海报、灯箱等的设计制作。

**三、我所了解的平面设计**

我认为可以做平面设计的人，都是有一定的审美的！他们还会有一定的美术功底才可以设计出让人赏心悦目的设计。当然光有以上两个还不行，还要有会创新的脑子！我认为会做平面设计的人是很厉害的！无论是从什么角度来看。其实我喜欢的偶像在没入娱乐圈之前也是一位设计师，所以我一直对设计师都很喜爱。爱屋及乌。

**四、平面设计师的类别**

我所知道的是有：

1. 企划美工
2. 网站美工(也就是我这个专业比较常接触到的一种)
3. 平面美工
4. 陈列美工
5. 影楼美工
6. 喷绘美工
7. 舞台美工
8. 影视剧美工

**五、平面设计基础**

透视、构图、色彩基础、平面构成。

**六、成为平面设计师要用到的软件**

第一个是我很熟悉的Photoshop。这个软件在这个学期里面，我在宋歌老师的课上粗略地学过这个软件该怎么用，我也自己在B站上面自己搜索过一些视频在自己学习一些基础知识，以及在帮人拍完照修图的时候有百度搜过一些内容如何获得一些效果。

第二个是PageMaker。我作为一个网页设计的外行人士，其实听都没有听过这个软件的名字，所以就不加以介绍了。

**放一个我觉得很好看的好看的设计图**

![通过none值进行隐藏](/assets/images/pmsj9.jpg)