---
title: "圈圈圆圆圈圈"
excerpt_separator: "<!--more-->"
categories:
  - SVG笔记
tags:
  - H5动画
  - SVG
  - 标识
---
！！！！

<!--more-->

### 圈圈圆圆圈圈

---

<style>
.frysvg{
	animation: bacon 3s infinite;
}

@keyframes bacon
{0% {fill:red;}
50% {fill:gold;}
100% {fill:green;}
}
</style>





<div class="frysvg">

<svg xmlns="http://www.w3.org/2000/svg" version="1.1">
   <circle cx="100" cy="50" r="40" stroke="black" stroke-width="2"  />
</svg> 
</div>  

