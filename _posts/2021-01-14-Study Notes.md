---
layout: page
title: Jekyll是什么？为什么要学习写博客？
excerpt_separator: "<!--more-->"
categories:
     - 学习笔记
---
！！！

<!--more-->

## Jekyll是什么？

Jekyll是运行在Ruby平台上的程序。Jekyll是一个简单的免费的Blog（博客）生成工具，是一个生成静态网页的工具，不需要数据库支持，并且Jekyll可以免费部署在Github上，还可以绑定自己的域名，是一个很好的博客系统。

可以在这里学习[Jekyll的基本用法](https://wiki.jikexueyuan.com/project/jekyll/usage.html)

## 为什么要学习Jekyll写博客？

1. **是提高叙事能力的一种方式**
2. **很好的锻炼思维和组织总结能力的方式**
3. **积累自己的方式**
4. **为他人提供解决方案**
5. **可以促进自己的成长**



## 用Jekyll写博客

首先要先建一个文章文件夹。

> 所有的文章都在`_posts`文件夹中

然后要创建文章的文件

> 要注意文章的命名要遵循（年-月-日-标题）的格式。因为之前我自己有出现过文章显现不出来的情况，原因就是文章的命名是不正确的。

内容格式

> 所有博客文章顶部必须有一段YAML头信息。

最后便写出自己想写的内容就好啦！当然也要注意语言问题！Jekyll支持2种流行的标记语言格式：Markdown和 Textile。而我们现在最常用的就是Markdown语法！

当然写文章的时候也要注意语法问题哦！

[Markdown](https://www.runoob.com/markdown/md-tutorial.html)

[Textile](https://www.lfhacks.com/index.php?id=12)